package es.unex.asee.frojomar.asee_ses.repository.room.roomdb;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import es.unex.asee.frojomar.asee_ses.model.Center;
import es.unex.asee.frojomar.asee_ses.model.City;

@Dao
public interface CitiesDAO {

    @Query("SELECT * FROM Cities")
    public LiveData<List<City>> getAll();

    @Query("SELECT * FROM Cities WHERE id=:id")
    public LiveData<City> getById(Integer id);

    @Query("SELECT * FROM Cities LIMIT 1")
    public List<City> existData();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(City item);

    @Update
    public void update(City item);

    @Query("DELETE FROM Cities")
    public void deleteAll();
}
